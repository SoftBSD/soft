use anyhow::{anyhow, Context, Result};
use duct::cmd;
use std::path::Path;

pub fn run_contained(path: &Path, cmd: &str) -> Result<()> {
    let name_arg = format!(
        "name=soft:{}",
        path.file_name()
            .ok_or(anyhow!("Path weirdness"))?
            .to_str()
            .ok_or(anyhow!("Path unicode weirdness"))?
    );
    cmd!(
        "jail",
        "-c",
        format!("path={}", path.display()),
        name_arg,
        "ip4=inherit",
        "ip6=inherit",
        "sysvmsg=new",
        "sysvsem=new",
        "sysvshm=new",
        "exec.clean",
        "command=/bin/sh",
        "-c",
        cmd,
    )
    .run()
    .with_context(|| format!("Failed to run command '{}'", cmd))?;
    Ok(())
}
