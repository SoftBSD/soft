use anyhow::{Context as _, Result};
use duct::cmd;
use std::path::Path;

// TODO: rewrite to not use rsync, with cap-std(?)
pub fn sync_files(from: &Path, to: &Path) -> Result<()> {
    log::trace!("Syncing files from '{}' to '{}'", from.display(), to.display());
    cmd!("rsync", "-caAXUHO", "--delete", from, to)
        .run()
        .with_context(|| format!("Failed to sync files from '{}' to '{}'", from.display(), to.display()))?;
    Ok(())
}
